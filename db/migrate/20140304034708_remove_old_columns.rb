class RemoveOldColumns < ActiveRecord::Migration
  def change
    remove_column :users, :pet_poison
    remove_column :users, :red_it
  end
end
