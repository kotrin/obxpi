class CreateObxDatabase < ActiveRecord::Migration
  def change
    # These are extensions that must be enabled in order to support this database
    enable_extension "plpgsql"

    create_table "activity_logs", force: true do |t|
      t.integer  "user_id"
      t.string   "record_type"
      t.integer  "record_id"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "cases", force: true do |t|
      t.text      "responses"
      t.text      "beneficiaries"
      t.string    "product_name"
      t.string    "product_state",           limit: 2
      t.string    "source_name"
      t.uuid      "case_uuid"
      t.string    "signed_documents_object",                         default: ""
      t.text      "invalid_rule_names"
      t.timestamp "created_at",                        precision: 6
      t.timestamp "updated_at",                        precision: 6
    end

    create_table "forums", force: true do |t|
      t.string   "name"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "market_items", force: true do |t|
      t.integer  "gabloons",      default: 0, null: false
      t.integer  "frammes",       default: 0, null: false
      t.string   "name"
      t.string   "description"
      t.string   "action_method"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "mentions", force: true do |t|
      t.integer  "user_id"
      t.datetime "viewed_at"
      t.integer  "mentionable_id"
      t.string   "mentionable_type"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "pages", force: true do |t|
      t.integer  "user_id"
      t.text     "markdown"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.text     "style"
    end

    create_table "posts", force: true do |t|
      t.text     "body"
      t.integer  "topic_id"
      t.integer  "user_id"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "shouts", force: true do |t|
      t.text     "body"
      t.integer  "user_id"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "topics", force: true do |t|
      t.string   "name"
      t.text     "body"
      t.integer  "forum_id"
      t.integer  "user_id"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "users", force: true do |t|
      t.string   "email",                  default: "",    null: false
      t.string   "encrypted_password",     default: "",    null: false
      t.string   "reset_password_token"
      t.datetime "reset_password_sent_at"
      t.datetime "remember_created_at"
      t.integer  "sign_in_count",          default: 0,     null: false
      t.datetime "current_sign_in_at"
      t.datetime "last_sign_in_at"
      t.string   "current_sign_in_ip"
      t.string   "last_sign_in_ip"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "admin",                  default: false, null: false
      t.string   "name"
      t.integer  "gabloons",               default: 0,     null: false
      t.string   "avatar_file_name"
      t.string   "avatar_content_type"
      t.integer  "avatar_file_size"
      t.datetime "avatar_updated_at"
      t.datetime "last_seen"
      t.integer  "edits",                  default: 0,     null: false
      t.string   "pet"
      t.integer  "pet_poison",             default: 0,     null: false
      t.boolean  "red_it",                 default: false, null: false
    end

    add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
    add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

    create_table "votes", force: true do |t|
      t.integer  "user_id"
      t.integer  "topic_id"
      t.integer  "post_id"
      t.datetime "created_at"
      t.datetime "updated_at"
    end
  end
end
