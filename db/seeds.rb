# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.create(email: 'robertpeterson@gmail.com', password: 'asdasdasd', password_confirmation: 'asdasdasd', admin: true, name: "robert")
user2 = User.create(email: 'jon@tanner.com', password: 'testing123', password_confirmation: 'testing123', admin: true, name: "tanman")

forum = Forum.create(name: "General")
forum.topics.create(name: "Topic!", body: "sup boy", user_id: user.id)
forum.topics.create(name: "Some pretty general shit", body: "Holler at a son", user_id: user.id)
forum.topics.create(name: "My name is Jorbo, can I meet you today?", body: "sup boy", user_id: user.id)
forum.topics.create(name: "DAE like to go to Reddit?  LOL", body: "Wut a great website XD", user_id: user.id)

forum = Forum.create(name: "Politics and Bullshit")
forum.topics.create(name: "NOBAMA NOBAMA NOBAMA", body: "He wasn't even born on earth, idiots.", user_id: user.id)
forum.topics.create(name: "I love politics, lets talk about how to jerk each other off", body: "Yummy J/O advice!", user_id: user.id)
forum.topics.create(name: "DAE RON PAUL?", body: "Raaaaaawn Paaaaaaaul!", user_id: user.id)
forum.topics.create(name: "My Clinton shakes bring all the Bills to the yard", body: "And they be all like haha wut.", user_id: user.id)