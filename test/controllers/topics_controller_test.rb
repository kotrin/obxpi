require_relative '../test_helper'

class TopicsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  context "create" do
    should "add topic" do
      user = FactoryGirl.create(:user)
      forum = FactoryGirl.create(:forum)
      sign_in user
      assert_difference "Topic.count", 1 do
        post :create, forum_id: forum.id, topic: { name: "Science", body: "it is neat" }
      end
    end
    
    should "require sign in" do
      forum = FactoryGirl.create(:forum)
      assert_difference "Topic.count", 0 do
        post :create, forum_id: forum.id, topic: { name: "Science", body: "it is neat" }
      end
      assert_response :redirect
    end
  end
  
  context "show" do
    should "not require sign in" do
      topic = FactoryGirl.create(:topic)
      get :show, id: topic.id, forum_id: topic.forum.id
    end
  end
  
  context "edit" do
    should "render for user with edits" do
      sign_in FactoryGirl.create(:user, edits: 1)
      topic = FactoryGirl.create(:topic)
      get :edit, forum_id: topic.forum.id, id: topic.id
      assert_response :success
    end
    
    should "not render for user with no edits" do
      sign_in FactoryGirl.create(:user, edits: 0)
      topic = FactoryGirl.create(:topic)
      get :edit, forum_id: topic.forum.id, id: topic.id
      assert_response :redirect
    end
  end
  
  context "update" do
    should "change record if user has edits" do
      topic = FactoryGirl.create(:topic, body: "hi")
      user = FactoryGirl.create(:user, edits: 1)
      sign_in user
      put :update, forum_id: topic.forum.id, id: topic.id, topic: { body: "science" }
      topic.reload
      assert_equal "science", topic.body
      user.reload
      assert_equal 0, user.edits
    end
    
    should "not change record if user does not have edits" do
      topic = FactoryGirl.create(:topic, body: "hi")
      user = FactoryGirl.create(:user, edits: 0)
      sign_in user
      put :update, forum_id: topic.forum.id, id: topic.id, topic: { body: "science" }
      topic.reload
      assert_equal "hi", topic.body
      user.reload
      assert_equal 0, user.edits
    end
  end
end
