require_relative '../test_helper'

class AdminControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  setup do
    sign_in FactoryGirl.create(:user, :admin)
  end
  
  context "GET index" do
    should "redirect when not admin user" do
      sign_in FactoryGirl.create(:user)
      get :index
      assert_response :redirect
    end
  end
  
  context "make" do
    should "set user to admin" do
      user = FactoryGirl.create(:user)
      refute user.admin?
      get :make, user_id: user.id
      assert user.reload.admin?
    end
  end
  
  context "remove" do
    should "user admin privs" do
      user = FactoryGirl.create(:user, :admin)
      assert user.admin?
      get :remove, user_id: user.id
      refute user.reload.admin?
    end
  end
end
