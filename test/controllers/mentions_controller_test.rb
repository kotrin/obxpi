require_relative '../test_helper'

class MentionsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  setup do
    @user = FactoryGirl.create(:user, name: "steve")
    sign_in @user
  end
  
  context "GET /show" do
    context "for a mention in a shout" do
      should "redirect to shout show" do
        shout = FactoryGirl.create(:shout, body: "hi @steve how are you?")
        Mention.generate(shout)
        mention = shout.mentions.first
        get :show, id: mention.id
        assert_response :redirect
        assert_redirected_to shout_path(shout)
        assert_equal 0, @user.reload.mentions.unseen.count
      end
    end
    
    context "for a mention in a post" do
      should "redirect to posts topic" do
        post = FactoryGirl.create(:post, body: "hi @steve lawl")
        Mention.generate(post)
        mention = post.mentions.first
        get :show, id: mention.id
        assert_response :redirect
        assert_redirected_to forum_topic_path(post.topic.forum, post.topic)
        assert_equal 0, @user.reload.mentions.unseen.count
      end
    end
    
    context "for a mention in a topic" do
      should "redirect to topic" do
        topic = FactoryGirl.create(:topic, body: "buy more @steve and you win a @steve")
        Mention.generate(topic)
        mention = topic.mentions.first
        get :show, id: mention.id
        assert_response :redirect
        assert_redirected_to forum_topic_path(topic.forum, topic)
        assert_equal 1, @user.reload.mentions.unseen.count
      end
    end
  end
  
end
