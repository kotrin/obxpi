require_relative '../test_helper'

class UsersControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  setup do
    @user = FactoryGirl.create(:user, name: "steve")
    sign_in @user
  end
  
  context "update" do
    should "update logged in user" do
      post :update, id: @user.id, user: { name: "newName" }
      assert_response :redirect
      assert_equal "newName", @user.reload.name
    end
    
    should "not update other user" do
      user = FactoryGirl.create(:user, name: "dave")
      post :update, id: user.id, user: { name: "nopeName" }
      assert_equal "dave", user.reload.name
      assert_response :redirect
    end
  end
end