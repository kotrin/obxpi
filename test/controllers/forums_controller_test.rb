require_relative '../test_helper'

class ForumsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  setup do
    user = FactoryGirl.create(:user, name: "steve")
    sign_in user
  end
  
  should "have index" do
    get :index
    assert_response :success
    assert_template :index
  end
  
  should "have show" do
    forum = FactoryGirl.create(:forum)
    get :show, id: forum.id
    assert_response :success
    assert_template :show
  end
  
  context "create" do
    should "require sign in" do
      post :create
      assert_response :redirect
    end
    
    should "require admin" do
      sign_in FactoryGirl.create(:user)
      post :create
      assert_response :redirect
    end
    
    should "create new forum" do
      sign_in FactoryGirl.create(:user, :admin)
      assert_difference 'Forum.count', 1 do
        post :create, forum: { name: "New Forum" }
      end
    end
  end
end
