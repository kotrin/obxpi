require_relative '../test_helper'

class ForumTest < ActiveSupport::TestCase
  should validate_presence_of(:name)
  should have_many(:topics)
  should have_many(:posts).through(:topics)
end
