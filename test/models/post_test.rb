require_relative '../test_helper'

class PostTest < ActiveSupport::TestCase
  should validate_presence_of(:body)
  should belong_to(:user)
  should belong_to(:topic)
  should have_many(:mentions)
end
