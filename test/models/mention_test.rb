require_relative '../test_helper'

class MentionTest < ActiveSupport::TestCase
  should belong_to(:user)
  should belong_to(:mentionable)
  
  context ".generate" do
    should "create mention records for all mentioned usernames" do
      user1 = FactoryGirl.create(:user)
      user2 = FactoryGirl.create(:user)
      shout = FactoryGirl.create(:shout, user: user1, body: "hi @#{user2.name} i am @#{user1.name} and @bloop is no one")
      
      assert_difference 'Mention.count', 2 do
        Mention.generate(shout)
      end
    end
  end
end
