require_relative '../test_helper'

class ShoutTest < ActiveSupport::TestCase
  should validate_presence_of(:body)
  should belong_to(:user)
  should have_many(:mentions)
end
