class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  has_many :shouts
  has_many :topics
  has_many :posts
  has_many :mentions
  has_many :activity_logs
  has_many :votes
  
  has_one :page
  
  has_attached_file :avatar, styles: { thumb: "100x100>" }, default_url: "/images/:style/default.png"
  
  NAME_REGEX = /\A[a-zA-Z0-9_]+\Z/
  ILLEGAL_NAMES = %w{ admin www }
  PETS = %w{ :dog: :cat: :sheep: :snake: :frog: 
             :turtle: :penguin: :crocodile: :pig: 
             :chicken: :mouse: :wolf: :bear: :snowman: 
             :shell: :squirrel :ghost: :santa: :skull: }
  
  validates_presence_of :name
  validates_format_of :name, with: NAME_REGEX
  validates_length_of :name, maximum: 20
  validates_uniqueness_of :name
  validates :name, exclusion: { in: ILLEGAL_NAMES }
  
  validates_attachment_size :avatar, less_than: 1.megabyte
  validates_attachment_content_type :avatar, content_type: ['image/jpeg', 'image/png', 'image/gif']
  
  scope :online, -> { where("last_seen > ?", 5.minutes.ago) }
  
  def flag_mentions_as_viewed!
    mentions.unseen.each do |mention|
      mention.touch(:viewed_at)
    end
  end
  
  def give_edits(amount)
    update_attribute(:edits, self.edits + amount)
  end
  
  def take_edit!
    update_attribute(:edits, self.edits - 1)
  end
  
  def take_edits!(amount)
    update_attribute(:edits, self.edits - amount)
  end
  
  def poison_pet!
    update_attribute(:pet, "")
  end
  
  def generate_pet
    update_attribute(:pet, PETS[rand(PETS.length) - 1])
  end
  
  def last_activity
    activity_logs.order(created_at: :desc).first
  end
end
