class Mention < ActiveRecord::Base
  belongs_to :user
  belongs_to :mentionable, polymorphic: true
  
  scope :unseen, -> { where(viewed_at: nil) }

  def self.generate(obj)
    names = obj.body.scan(/[@][a-zA-Z0-9_]{1,20}/)
    names.each do |name|
      lookup_name = name[1..-1]
      if mention_user = User.where(name: lookup_name).first
        mention = obj.mentions.build(user: mention_user)
        mention.save
      end
    end
  end
  
end
