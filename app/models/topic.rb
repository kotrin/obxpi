class Topic < ActiveRecord::Base
  validates_presence_of :name, :body
  
  belongs_to :forum
  belongs_to :user
  
  has_many :posts
  has_many :mentions, as: :mentionable
  has_many :votes
  
  def last_post
    posts.order(updated_at: :desc).first
  end
  
  def user_has_voted?(user)
    self.votes.where(user_id: user.id).count > 0
  end
end
