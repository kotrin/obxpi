class Shout < ActiveRecord::Base
  validates_presence_of :body
  
  has_many :mentions, as: :mentionable
  
  belongs_to :user
  
  scope :latest, -> { order(created_at: :desc).limit(20) }
end
