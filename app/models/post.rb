class Post < ActiveRecord::Base
  validates_presence_of :body
  
  belongs_to :topic
  belongs_to :user
  
  has_many :mentions, as: :mentionable
  has_many :votes
  
  def user_has_voted?(user)
    self.votes.where(user_id: user.id).count > 0
  end
end
