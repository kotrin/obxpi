class Forum < ActiveRecord::Base
  validates_presence_of :name
  
  has_many :topics
  has_many :posts, through: :topics
  
  def last_activity
    post = self.posts.order(updated_at: :desc).first
    topic = self.topics.order(updated_at: :desc).first
    
    return post unless topic
    return topic unless post
    
    if post.updated_at > topic.updated_at
      post
    else
      topic
    end
  end
end
