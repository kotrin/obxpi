class PagesController < ApplicationController
  before_filter :authenticate_user!, except: [:show]
  before_filter :ensure_user_is_self, except: [:show]
  
  def create
    markdown = "# #{current_user.name}\n\nmy [obx.io](http://obx.io) page"
    style = "body {\n  color: #333;\n  background: #eee;\n  text-align: center;\n  font-size: 25px;\n  margin-top: 50px;\n  font-family: verdana;\n}"
    style += "\na {\n  color: #9a0000;\n}"
    page = Page.new(markdown: markdown, style: style, user_id: user_id)
    if !current_user.page && page.save
      ActivityLog.log('page', page.id, current_user)
      flash[:notice] = "A new page! Hurray!"
    else
      flash[:notice] = "Making a new page totally broke."
    end
    redirect_to edit_user_page_path(user_id)
  end
  
  def show
    user = find_user_by_name_or_id
    if user
      if user.page 
        @page = user.page.formatted_markdown
        @user = user
        @style = user.page.formatted_style
        render '/pages/show', layout: "user_page"
      else
        @page = "User has not created their page yet! LAME"
      end
    else
      @page = "User not found"
    end
  end
  
  def destroy
    page_id = current_user.page.id
    if current_user.page.destroy
      flash[:notice] = "Your page has been DESTROYED, MUAHAHAHAHA"
    else
      flash[:notice] = "Trying to destroy your page failed. Whoops."
    end
    redirect_to edit_user_page_path(user_id)
  end
  
  def update
    page = current_user.page.update_attributes(page_params)
    redirect_to edit_user_path(user_id)
  end
  
  def edit
    @user = current_user
  end
  
  private
  
  def ensure_user_is_self
    redirect_to root_path unless current_user.id.to_s == user_id
  end
  
  def page_params
    params.require(:page).permit(:markdown, :style)
  end
  
  def user_id
    params[:user_id]
  end
  
  def user_name
    params[:user_name]
  end
  
  def find_user_by_name_or_id
    return User.where(name: user_name).first if user_name
    User.find(user_id)
  end
end
