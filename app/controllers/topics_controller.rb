class TopicsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :ensure_user_has_edits, only: [:edit, :update]
  
  def create
    forum = Forum.find(forum_id)
    topic = forum.topics.new(topic_params.merge(user: current_user))
    if topic.save
      Mention.generate(topic)
      ActivityLog.log('topic', topic.id, current_user)
      current_user.give_edits(1)
      flash[:notice] = "Topic created! Booyah."
    else
      flash[:notice] = "That topic could not be created. Try again?"
    end
    redirect_to forum_path(forum)
  end
  
  def show
    @topic = Forum.find(forum_id).topics.find(topic_id)
    @posts = @topic.posts.order(created_at: :desc)
  end
  
  def edit
    @topic = Topic.find(topic_id)
  end
  
  def update
    topic = Topic.find(topic_id)
    if topic.update_attributes(topic_params)
      Mention.generate(topic)
      current_user.take_edit!
    end
    redirect_to forum_topic_path(topic.forum, topic)
  end
  
  def vote
    topic = Topic.find(topic_id)
    if topic.user_has_voted?(current_user)
      flash[:notice] = "You already voted!"
    else
      if current_user.edits >= 1
        current_user.take_edit!
        topic.user.give_edits(1)
        flash[:notice] = "Your vote has been counted."
        Vote.create(user: current_user, topic: topic)
      else
        flash[:notice] = "You need at least 1 edit to vote!"
      end
    end
    redirect_to return_to_location
  end
  
  private
  
  def topic_params
    params.require(:topic).permit(:name, :body)
  end
  
  def forum_id
    params[:forum_id]
  end
  
  def topic_id
    params[:id]
  end
end
