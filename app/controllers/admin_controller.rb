class AdminController < ApplicationController
  before_filter :authenticate_user!
  before_filter :admin_user?
  
  def index
    @users = User.order(admin: :desc)
    @forums = Forum.all
  end
  
  def make
    user = User.find(user_id)
    user.admin = true
    user.save
    redirect_to admin_path
  end
  
  def remove
    user = User.find(user_id)
    user.admin = false
    user.save
    redirect_to admin_path
  end
  
  private
  
  def user_id
    params[:user_id]
  end
end
