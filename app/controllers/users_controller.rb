class UsersController < ApplicationController
  before_filter :authenticate_user!, except: [:index]
  
  def index
    @users = User.order(:created_at).all
  end
  
  def show
    @user = User.find(user_id)
    @mentions = @user.mentions.order(created_at: :desc)
  end
  
  def edit
    @user = current_user
  end
  
  def update
    user = current_user
    if user.id.to_s == user_id.to_s
      new_params = user_params
      if new_params[:password].blank?
         new_params.delete("password")
         new_params.delete("password_confirmation")
       end
      if user.update_attributes(new_params)
        sign_in user, bypass: true
        flash[:notice] = "Your account was updated!"
      else
        flash[:notice] = "Bad stuff happened and nothing saved."
      end
      redirect_to edit_user_path(user)
    else
      redirect_to root_path
    end
  end
  
  def info
    @users_online = User.online.count
    @mentions = current_user.mentions.unseen.count
    respond_to do |format|
      format.json
    end
  end
  
  def generate_pet
    if current_user.edits >= 5
      user = User.find(user_id)
      user.take_edits!(5)
      user.generate_pet
    end
    redirect_to session[:return_to]
  end
  
  def poison_pet
    if current_user.edits >= 10
      user = User.find(user_id)
      if user.poison_pet!
        current_user.take_edits!(10)
      end
    end
    redirect_to session[:return_to]
  end
  
  private
  
  def user_id
    params[:id]
  end
  
  def user_name
    params[:user_name]
  end
  
  def user_params
    params.require(:user).permit(:name, :avatar, :password, :password_confirmation)
  end
end
