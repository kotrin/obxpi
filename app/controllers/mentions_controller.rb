class MentionsController < ApplicationController
  def show
    mention = Mention.find(params[:id])
    mention.touch(:viewed_at)
    case mention.mentionable
    when Shout
      redirect_to shout_path(mention.mentionable)
    when Topic
      redirect_to forum_topic_path(mention.mentionable.forum, mention.mentionable)
    when Post
      redirect_to forum_topic_path(mention.mentionable.topic.forum, mention.mentionable.topic)
    end
  end
end
