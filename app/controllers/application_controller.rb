class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  after_filter :store_location, if: :should_store_location?
  after_filter :user_seen, if: :user_signed_in?
  
  helper_method :user_has_edits?
  
  def user_has_edits?
    user_signed_in? && current_user.edits?
  end
  
  private
  
  def ensure_user_has_edits
    redirect_to root_path unless user_has_edits?
  end
  
  def should_store_location?
    !(request.fullpath =~ /\.json/)
  end
  
  def store_location
    session[:return_to] = request.fullpath
  end

  def clear_stored_location
    session[:return_to] = nil
  end
  
  def return_to_location
    session[:return_to]
  end
  
  def admin_user?
    redirect_to(root_path) unless current_user.admin?
  end
  
  def user_seen
    current_user.touch(:last_seen)
  end
  
  # Devise overrides
  
  def after_sign_in_path_for(resource)
    return_location = return_to_location
    clear_stored_location if return_location
    return_location || root_path
  end
  
  def after_sign_out_path_for(resource)
    request.referrer || root_path
  end
end
