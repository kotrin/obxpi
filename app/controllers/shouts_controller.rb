class ShoutsController < ApplicationController
  before_filter :ensure_user_has_edits, only: [:edit, :update]
  before_filter :authenticate_user!, except: [:index]
  
  def index
    @shouts = Shout.latest
    
    respond_to do |format|
      format.json
      format.html
    end
  end
  
  def edit
    @shout = Shout.find(shout_id)
  end
  
  def show
    @shout = Shout.find(shout_id)
    @before_shouts = Shout.where(['id > ? and id < ?', @shout.id, @shout.id+4])
    @after_shouts = Shout.where(['id < ? and id > ?', @shout.id, @shout.id-5])
  end
  
  def update
    shout = Shout.find(shout_id)
    if shout.update_attributes(shout_params)
      Mention.generate(shout)
      current_user.take_edit!
    end
    redirect_to shouts_path
  end
  
  def create
    shout = current_user.shouts.new(shout_params)
    if shout.save
      Mention.generate(shout)
      ActivityLog.log('shout', shout.id, current_user)
      current_user.give_edits(1)
      flash[:notice] = "You are shouting. And letting it all outing."
    else
      flash[:notice] = "Your shout did not save because I do not know why."
    end
    redirect_to shouts_path
  end
  
  private
  
  def shout_id
    params[:id]
  end
  
  def shout_params
    params.require(:shout).permit(:body)
  end
end
