json.activity_log do
  json.array! @activity_log do |activity_log|
    json.avatar_url activity_log.user.avatar.url(:thumb)
    json.body format_activity_log(activity_log)
  end
end

json.mentions do
  json.array! @mentions do |mention|
    json.body "Mentioned #{mentionable_summary(mention)}"
  end
end