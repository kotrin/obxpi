# ObX

<http://obx.io>

These codes make the website above. Our magical magics make merry majestic mops.

## Dependencies

- Ruby 2.0+
- PostgreSQL

## Elephant

                         
                 ___.-~"~-._   __....__
               .'    `    \ ~"~        ``-.
              /` _      )  `\              `\
             /`  a)    /     |               `\
            :`        /      |                 \
       <`-._|`  .-.  (      /   .            `;\\
        `-. `--'_.'-.;\___/'   .      .       | \\
     _     /:--`     |        /     /        .'  \\
    ("\   /`/        |       '     '         /    :`;
    `\'\_/`/         .\     /`~`=-.:        /     ``
      `._.'          /`\    |      `\      /(
                    /  /\   |        `Y   /  \
                   J  /  Y  |         |  /`\  \
                  /  |   |  |         |  |  |  |
                 "---"  /___|        /___|  /__|
                        '"""         '"""  '"""

## How do?

Install the gems!

    bundle install

Create the datasbases!

    bundle exec rake db:create

Migrate the datasbases!

    bundle exec rake db:migrate

Start the server!

    bundle exec rails s

Look at the seed file for infos on how to make admin users and whatsnots. Math!
