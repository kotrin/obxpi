# Be sure to restart your server when you modify this file.

if Rails.env.production?
  Obxpi::Application.config.session_store :cookie_store, key: '_obxpi_session1', domain: '.obx.io'
else
  Obxpi::Application.config.session_store :cookie_store, key: '_obxpi_session1'
end