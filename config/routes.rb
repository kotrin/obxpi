Obxpi::Application.routes.draw do
  
  devise_for :users, controllers: { registrations: 'users/registrations' }
  
  get "user_info", to: "users#info"
  get "poison_pet/:id", to: "users#poison_pet", as: :poison_pet
  get "generate_pet/:id", to: "users#generate_pet", as: :generate_pet
  resources :users do
    resource :page
  end
  
  resources :forums do
    resources :topics do
      get "vote", on: :member
      resources :posts do
        get "vote", on: :member
      end
    end
  end
  
  resources :mentions
  
  resources :shouts
  
  post "shout", to: "misc#shout", as: :misc_shout
  
  get "admin", to: "admin#index"
  get "admin/:user_id/make", to: "admin#make", as: :make_admin
  get "admin/:user_id/remove", to: "admin#remove", as: :remove_admin
  get "admin/edit_user/:user_id", to: "admin#edit_user", as: :edit_user_admin
  patch "admin/update_user/:user_id", to: "admin#update_user", as: :update_user_admin
  
  get "u/:user_name", to: "pages#show", as: :u_page
  
  get "online_users", to: "home#online_users", as: :online_users
  get "activity_log", to: "home#activity_log"
  get "stream", to: "home#stream"
  
  root to: "shouts#index"
  
end
