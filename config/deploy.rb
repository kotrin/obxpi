# config valid only for Capistrano 3.1
# lock '3.1.0'
# 
# set :ssh_options, {
#   forward_agent: true,
#   user: 'kotrin'
# }
# 
# role :app, %w{obx.io}
# 
# namespace :foreman do
#   desc "Start the application services"
#   task :start do
#     on roles(:app) do
#       run "sudo service obx_app start"
#     end
#   end
# 
#   desc "Stop the application services"
#   task :stop do
#     on role(:app) do
#       run "sudo service obx_app stop"
#     end
#   end
# 
#   desc "Restart the application services"
#   task :restart do
#      on roles(:app) do
#       run "sudo service obx_app restart"
#     end
#   end
# end